-- Variáveis

print("(papapa = bababa)")
papapa = "bababa"

-- Usando print para mostrar uma variável

print("print(papapa) foi executado abaixo!")
print(papapa)

-- Contas com variáveis

print("duasvezesdois = 2 * 2")
duasvezesdois = 2 * 2

print("print(duasvezesdois) foi executado abaixo!")
print(duasvezesdois)

-- Juntando duas variáveis(concatenando)

texto1 = "aiaiai"
texto2 = "uiui"
print(texto1 .. texto2)

-- Juntando duas variáveis(com espaço)

parte1 = "você é"
parte2 = "muito legal"

print(parte1 .. " " .. parte2)

-- Boolean

EstaAtivo = false
print(EstaAtivo)

-- == É o símbolo de comparação de igualdade

print(EstaAtivo == false)
print(EstaAtivo == true)

EstaInativo = true

print(EstaInativo)
print(EstaInativo == true)
print(EstaInativo == false)

-- Concatenação de strings e valores booleanos

feliz = true

-- Jeito errado:


print("print(fulano está feliz: " .. "feliz")

-- Jeito correto:

print("fulano está feliz: " .. tostring(feliz))

-- Acabouuuuuuuu
