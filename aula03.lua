-- Números negativos

ContaLegal = 30 - 100
print(ContaLegal)

-- Números negativos com casas decimais

Numero = -70.5848
print(Numero)

-- Valores nulos

Valor = nil

print(Valor)

-- Concatenando

print("O valor é: " .. tostring(Valor))

-- Escape de texto

frase1 = "Você é \"rico\""
print(frase1)

-- Texto com várias linhas

Texto = "Texto com duas\nlinhas"
print(Texto)

-- Parágrafo

Texto1 = "\tParágrafo!"
print(Texto1)

Duplo = "\t\tDuplo parágrafo!!"
print(Duplo)
