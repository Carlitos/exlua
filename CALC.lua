#!/bin/lua

while(true) do
	print("Olá! Bem vindo(a) a CALC, a Calculadora Aritimética em Lua do Carlitos! Eu sou o robô C.F.S.I.! O robô Calculadora de Funções Super Inteligentes!")

	print("Qual operação gostaria de realizar?")
	print("1) adição")
	print("2) subtração")
	print("3) multiplicação")
	print("4) divisão")
	print("5) raiz quadrada")
	print("6) ao quadrado")
	print("7) ao cubo")
	
	opera = io.read()

	print("Ok! Qual será o primeiro número?")

	num1 = io.read()
	
	print("E qual será o segundo número?(coloque nada se a operação escolhida for 5, 6 ou 7)")
	 
	num2 = io.read()

	if(opera == "1") then resul = num1 + num2
	elseif(opera == "2") then resul = num1 - num2
	elseif(opera == "3") then resul = num1 * num2
	elseif(opera == "4") then resul = num1 / num2
	elseif(opera == "5") then resul = math.sqrt(num1)
	elseif(opera == "6") then resul = num1 * num1
	elseif(opera == "7") then resul = num1 * num1 * num1
	end

	print("Então... o seu resultado é... " .. resul .. "!")

	print("Você gostaria de fazer mais uma conta? digite 'não' se não quiser, digite qualquer outra coisa para sim.")

	conmais = io.read()

	if(conmais == "não")
	then
		break
	end
end
